'use strict';

var async = require('async');
var AWS = require('aws-sdk');
var gm = require('gm').subClass({imageMagick: true});
var util = require('util');

var s3 = new AWS.S3();

var imageType = 'jpg';
// Supported sizes
var sizes = {
  thumb: {
    major: 120,
    minor: 120
  },
  small: {
    major: 400,
    minor: 300
  }
};

function scaleWidthAndHeight(scale, size) {
  if (!sizes[scale]){
    throw new Error('Unsupported Scale: ' + scale);
  }

  var isPortrait = size.height > size.width;
  var maxWidth, maxHeight;
  if (isPortrait) {
    maxWidth = sizes[scale].minor;
    maxHeight = sizes[scale].major;
  } else {
    maxWidth = sizes[scale].major;
    maxHeight = sizes[scale].minor;
  }

  var scalingFactor = Math.min(
    maxWidth / size.width,
    maxHeight / size.height
  );

  return {
    width: Math.floor(scalingFactor * size.width),
    height: Math.floor(scalingFactor * size.height)
  };
}

function copyImage(destBucket, photoKey, s3Response, done) {
  var destKey = photoKey + '.jpg';
  console.log('Copying original photo to: ' + destKey);
  s3.putObject({
    Bucket: destBucket,
    Key: destKey,
    Body: s3Response.Body,
    ContentType: s3Response.ContentType
  }, done);
}

function scalePhoto(destBucket, photoKey, scale, s3Response, done) {
  async.waterfall([
    function transform(next) {
      console.log('Resizing image to:' + scale);
      gm(s3Response.Body).size(function(err, size) {
        if (err) {
          return next(err);
        }
        var scaledSize = scaleWidthAndHeight(scale, size);
        console.log(scaledSize.width, ' px ', scaledSize.height, ' px ');
        this.resize(scaledSize.width, scaledSize.height)
          .toBuffer(imageType, next);
      });
    },
    function save(data, next) {
      var destKey = photoKey + '-' + scale + '.jpg';
      console.log('Saving scaled image to: ', destKey);
      s3.putObject({
        Bucket: destBucket,
        Key: destKey,
        Body: data,
        ContentType: s3Response.ContentType
      }, next);
    }
  ], function(err) {
    done(err);
  });
}

function createDestinationBaseKey(srcKey) {
  var keyPieces = srcKey.split('/');
  if (keyPieces.length !== 4) {
    return null;
  }
  //0 - inspection-photos
  //1 - src
  //2 - {inspection-id}
  //3 - {photo-id-1}.jpg
  var inspectionsPhotosKey = keyPieces[0];
  var inspectionId = keyPieces[2];
  var photoId = keyPieces[3].replace(/\.jpg/g, '');

  return inspectionsPhotosKey + '/' + inspectionId + '/' + photoId;
}


exports.handler = function(event, context) {
  // Read options from the event.
  console.log('Reading options from event:\n', util.inspect(event, {depth: 5}));
  var srcBucket = event.Records[0].s3.bucket.name;
  var srcKey = event.Records[0].s3.object.key;

  var destBaseKey = createDestinationBaseKey(srcKey);
  if (!destBaseKey) {
    context.fail(new Error('Ivalid source key format: ' + srcKey));
  }
  console.log('destBaseKey: ' + destBaseKey);

  async.waterfall([
    // Execute the following functions in sequence
    function download(next) {
      console.log('Getting image...');
      s3.getObject({
        Bucket: srcBucket,
        Key: srcKey
      }, next);
    },
    function processImage(response, next) {
      console.log('Got image, processing...');
      async.parallel([
        // Execute the following functions in parallel
        function (done) {
          copyImage(srcBucket, destBaseKey, response, done);
        },
        function (done) {
          scalePhoto(srcBucket, destBaseKey, 'thumb', response, done);
        },
        function (done) {
          scalePhoto(srcBucket, destBaseKey, 'small', response, done);
        }
      ], next);
    }
  ], function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log('Success!!!');
    }
    context.done(err);
  });
};
