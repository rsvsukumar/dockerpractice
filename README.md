image-resizer
=============

Lambda function to resize inspection images placed into an S3 bucket.

###Bucket Structure
- {bucket-name}
  - inspection-photos
    - src
      - {inspection-id}
        - {photo-id-1}.jpg
        - {photo-id-2}.jpg
        - ...
    - {inspection-id}
      - {photo-id-1}.jpg
      - {photo-id-1}-small.jpg
      - {photo-id-1}-thumb.jpg
      - {photo-id-2}.jpg
      - {photo-id-2}-small.jpg
      - {photo-id-2}-thumb.jpg
      - ...


###Deployment
- Create a zip from within the folder
  `$ zip -vr folder.zip ./ -x "*.DS_Store" "*.git*`
- Upload to S3
